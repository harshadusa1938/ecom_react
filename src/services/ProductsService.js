import axios from 'axios';

const PRODUCTS_API_BASE_URL = "http://localhost:8081/tms/v1/api/prdts/products";

class ProductsService {

    getProducts(){
        return axios.get(PRODUCTS_API_BASE_URL);
    }
    getProductsById(productsId){
        return axios.get(PRODUCTS_API_BASE_URL + '/' + productsId);
    }

}

export default new ProductsService()