import axios from 'axios'

const LOCAL_API_URL = 'http://localhost:8081'
const TASK_PAGE = 'tms/v1/api/tasks'
const GETTASK = 'getTask'
const INSTRUCTOR = 'getAllTask'
const ADDTASK = 'createTask'
const DELETETASK = 'deleteTask'
const UPDATETASK = 'updateTask'

const INSTRUCTOR_API_URL = `${LOCAL_API_URL}/${TASK_PAGE}`

class TaskDataService {
    retrieveAllTask(name) {
        return axios.get(`${INSTRUCTOR_API_URL}/${INSTRUCTOR}`);
    }
    
    getTask(id) {
        //return axios.get(INSTRUCTOR_API_URL + '/' + GETTASK + '/' + id);
        //return axios.get(INSTRUCTOR_API_URL + '/' + GET_TASK + '/' + id);
        return axios.get(INSTRUCTOR_API_URL + '/' + GETTASK + '/' + id);
    }

    deleteTask(id) {
        return axios.delete(INSTRUCTOR_API_URL + '/' + DELETETASK + '/' + id);
    }

    addTask(task) {
        return axios.post(INSTRUCTOR_API_URL + '/' + ADDTASK, task);
    }

    editTask(task, id) {
       return axios.put(INSTRUCTOR_API_URL + '/' + UPDATETASK + '/' + id, task);
       //return axios.put(INSTRUCTOR_API_URL + '/' + UPDATETASK + '/' + task);       
    }
}
export default new TaskDataService()