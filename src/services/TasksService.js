import axios from 'axios';

const TASKS_API_BASE_URL = "http://localhost:8081/tms/v1/api/tsks/tasks";

class TasksService {

    getTasks(){
        return axios.get(TASKS_API_BASE_URL);
    }
    createTasks(tasks){
        return axios.post(TASKS_API_BASE_URL, tasks);
    }

    getTasksById(tasksId){
        return axios.get(TASKS_API_BASE_URL + '/' + tasksId);
    }

    updateTasks(tasks, tasksId){
        return axios.put(TASKS_API_BASE_URL + '/' + tasksId, tasks);
    }

    deleteTasks(tasksId){
        return axios.delete(TASKS_API_BASE_URL + '/' + tasksId);
    }
}

export default new TasksService()