import axios from 'axios';

const EXPENSES_API_BASE_URL = "http://localhost:8081/tms/v1/api/exps/expenses";

class ExpensesService {

    getExpenses(){
        return axios.get(EXPENSES_API_BASE_URL);
    }

    createExpenses(expenses){
        return axios.post(EXPENSES_API_BASE_URL, expenses);
    }

    getExpensesById(expensesId){
        return axios.get(EXPENSES_API_BASE_URL + '/' + expensesId);
    }

    updateExpenses(expenses, expensesId){
        return axios.put(EXPENSES_API_BASE_URL + '/' + expensesId, expenses);
    }

    deleteExpenses(expensesId){
        return axios.delete(EXPENSES_API_BASE_URL + '/' + expensesId);
    }
}

export default new ExpensesService()