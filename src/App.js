import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
//import "./App.css";

import AuthService from "./services/auth.service";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/home.component";
import Profile from "./components/profile.component";
import BoardUser from "./components/board-user.component";
import BoardModerator from "./components/board-moderator.component";
import BoardAdmin from "./components/board-admin.component";
import ListTask from "./components/Task/ListTask";
import AddTaskComponent from "./components/Task/AddTaskComponent";
import EditTaskComponent from "./components/Task/EditTaskComponent";
import ViewTaskComponent from "./components/Task/ViewTaskComponent";
import CreateEmployeeComponent from "./components/Employee/CreateEmployeeComponent";
import ListEmployeeComponent from "./components/Employee/ListEmployeeComponent";
//import UpdateEmployeeComponent from "./components/Employee/UpdateEmployeeComponent";
import ViewEmployeeComponent from "./components/Employee/ViewEmployeeComponent";
import CreateExpensesComponent from "./components/Expenses/CreateExpensesComponent";
import ListExpensesComponent from "./components/Expenses/ListExpensesComponent";
import ViewExpensesComponent from "./components/Expenses/ViewExpensesComponent";
import ListUsersComponent from "./components/Users/ListUsersComponent";
import ListTasksComponent from "./components/Tasks/ListTasksComponent";
import ViewTasksComponent from "./components/Tasks/ViewTasksComponent";
import CreateTasksComponent from "./components/Tasks/CreateTasksComponent";
//import ListProductsComponent from "./components/Product/ListProductsComponent";
import ViewProductsComponent from "./components/Product/ViewProductsComponent";
import ListOfficeExpensesMasterComponent from "./components/OfficeExpensesMaster/ListOfficeExpensesMasterComponent";

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showModeratorBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
        showModeratorBoard: user.roles.includes("ROLE_MODERATOR"),
        showAdminBoard: user.roles.includes("ROLE_ADMIN"),
      });
    }
  }

  logOut() {
    AuthService.logout();
  }

  render() {
    const { currentUser, showModeratorBoard, showAdminBoard } = this.state;

    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/home"} className="navbar-brand">
            TMS
          </Link>
          <div className="navbar-nav mr-auto">
          
            {showModeratorBoard && (
              <li className="nav-item">
                <Link to={"/mod"} className="nav-link">
                  Moderator Board
                </Link>
              </li>
            )}

            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/admin"} className="nav-link">
                  Admin
                </Link>
              </li>
            )}

            {currentUser && (
              <li className="nav-item">
                <Link to={"/user"} className="nav-link">
                  User
                </Link>
              </li>
            )}
            
            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/users"} className="nav-link">
                  Users
                </Link>
              </li>
            )}

            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/employees"} className="nav-link">
                  Employees
                </Link>
              </li>
            )}

            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/expensesMaster"} className="nav-link">
                  Office Expenses Master
                </Link>
              </li>
            )}

            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/expenses"} className="nav-link">
                  Expenses
                </Link>
              </li>
            )}

            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/tasks"} className="nav-link">
                  Tasks
                </Link>
              </li>
            )}

          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  {currentUser.username}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="nav-link" onClick={this.logOut}>
                  Logout
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                  Sign Up
                </Link>
              </li>
            </div>
          )}
        </nav>

        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/home"]} component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
            <Route path="/user" component={BoardUser} />
            <Route path="/mod" component={BoardModerator} />
            <Route path="/admin" component={BoardAdmin} />
            <Route path="/task" component={ListTask} />
            <Route path="/add-task" component={AddTaskComponent} />
            <Route path="/edit-task" component={EditTaskComponent} />
            <Route path="/view-task" component={ViewTaskComponent} />
            <Route path = "/employees" component = {ListEmployeeComponent}></Route>
            <Route path = "/add-employee/:id" component = {CreateEmployeeComponent}></Route>
            <Route path = "/view-employee/:id" component = {ViewEmployeeComponent}></Route>
            <Route path = "/expenses" component = {ListExpensesComponent}></Route>
            <Route path = "/add-expenses/:id" component = {CreateExpensesComponent}></Route>
            <Route path = "/view-expenses/:id" component = {ViewExpensesComponent}></Route>
            <Route path = "/users" component = {ListUsersComponent}></Route>
            <Route path = "/tasks" component = {ListTasksComponent}></Route>
            <Route path = "/view-tasks/:id" component = {ViewTasksComponent}></Route>
            <Route path = "/add-tasks/:id" component = {CreateTasksComponent}></Route>
          {/*   <Route path = "/products" component = {ListProductsComponent}></Route> */}
            <Route path = "/view-products/:id" component = {ViewProductsComponent}></Route>
            <Route path = "/expensesMaster" component = {ListOfficeExpensesMasterComponent}></Route>
            
            {/* <Route path = "/update-employee/:id" component = {UpdateEmployeeComponent}></Route> */}
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
