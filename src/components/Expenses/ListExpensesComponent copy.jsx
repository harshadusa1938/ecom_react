//import React, { Component } from 'react'
import ExpensesService from '../../services/ExpensesService'
import MaterialTable from 'material-table';
import React, { useState, useEffect, Component } from 'react';

//class ListExpensesComponent extends Component {

function ListExpensesComponent() {
    // constructor(props) {
    //     super(props)

    //     this.state = {
    //         expenses: []
    //     }
    //     this.addExpenses = this.addExpenses.bind(this);
    //     this.editExpenses = this.editExpenses.bind(this);
    //     this.deleteExpenses = this.deleteExpenses.bind(this);
    // }

    // deleteExpenses(id){
    //     ExpensesService.deleteExpenses(id).then( res => {
    //         this.setState({expenses: this.state.expenses.filter(expenses => expenses.id !== id)});
    //    });
    //  }
    // viewExpenses(id){
    //     this.props.history.push(`/view-expenses/${id}`);
    // }
    // editExpenses(id){
    //     this.props.history.push(`/add-expenses/${id}`);
    // }

    // componentDidMount(){
    //     ExpensesService.getExpenses().then((res) => {
    //         this.setState({ expenses: res.data});
    //     });
    // }

    //  addExpenses() {
    //      this.props.history.push('/add-expenses/_add');
    //  };

    // render() {
        const [data, setData] = useState([])
        useEffect(() => {
            fetch("http://localhost:8081/tms/v1/api/exps/expenses")
              .then(resp => resp.json())
              .then(resp => {
                setData(resp)
              })
          }, [])

        return (
            // <div>
            //      <h2 className="text-center">Expenses List</h2>
            //       <div className = "row">
            //         <button className="btn btn-primary" onClick={this.addExpenses}> Add Expenses</button>
            //      </div>
            //      <br></br>
            //      <div className = "row">
            //             <table className = "table table-striped table-bordered">

            //                 <thead>
            //                     <tr>
            //                         <th> Expenses Name</th>
            //                         <th> Amount</th>
            //                         <th> Date</th>
            //                         <th> Reason For Expenses </th>
            //                         <th> Actions</th>
            //                     </tr>
            //                 </thead>
            //                 <tbody>
            //                     {
            //                         this.state.expenses.map(
            //                             expense => 
            //                             <tr key = {expense.id}>
            //                                  <td> {expense.expensesName} </td>   
            //                                  <td> {expense.amount}</td>
            //                                  <td> {expense.date}</td>
            //                                  <td> {expense.reasonForExpenses}</td>                                            
            //                                  <td>
            //                                      <button onClick={ () => this.editExpenses(expense.id)} className="btn btn-success">Update </button>
            //                                      <button style={{marginLeft: "10px"}} onClick={ () => this.deleteExpenses(expense.id)} className="btn btn-danger">Delete </button>
            //                                      <button style={{marginLeft: "10px"}} onClick={ () => this.viewExpenses(expense.id)} className="btn btn-info">View </button>
            //                                  </td>                                         
            //                             </tr>
            //                         )
            //                     }
            //                 </tbody>
            //             </table>

            //      </div>

            // </div>

       

     <div style={{ maxWidth: '100%' }}>
         {/* <div className = "row">
            { <button className="btn btn-primary" onClick={this.addExpenses} > Add Expenses</button> }
         </div>  */}

        <MaterialTable
          columns={[
            { title: 'Expenses Name', field: 'expensesName' },
            { title: 'Amount', field: 'amount' },
            { title: 'Date', field: 'date', type: 'numeric' },
            { title: 'Reason For Expenses', field: 'reasonForExpenses' }
          ]}
          
          data={data}
          title="Expenses List"
        />
      </div>
 
        )
    }
//}

export default ListExpensesComponent
