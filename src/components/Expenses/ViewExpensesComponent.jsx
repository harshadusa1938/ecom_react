import React, { Component } from 'react'
import ExpensesService from '../../services/ExpensesService'

class ViewExpensesComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            expenses: {}
        }
    }

    componentDidMount(){
        ExpensesService.getExpensesById(this.state.id).then( res => {
            this.setState({expenses: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Expenses Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> Expenses Name: </label>
                            <div> { this.state.expenses.expensesName }</div>
                        </div>
                        <div className = "row">
                            <label> Amount: </label>
                            <div> { this.state.expenses.amount }</div>
                        </div>
                        <div className = "row">
                            <label> Date: </label>
                            <div> { this.state.expenses.date }</div>
                        </div>
                        <div className = "row">
                            <label> Reason For Expenses: </label>
                            <div> { this.state.expenses.reasonForExpenses }</div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default ViewExpensesComponent
