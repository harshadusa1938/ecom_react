import React, { Component } from 'react'
import ExpensesService from '../../services/ExpensesService';

class CreateExpensesComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            expensesName: '',
            amount: '',
            date: '',
            reasonForExpenses: ''
        }
        this.changeExpensesNameHandler = this.changeExpensesNameHandler.bind(this);
        this.changeAmountHandler = this.changeAmountHandler.bind(this);
        this.changeDateHandler = this.changeDateHandler.bind(this);
        this.changeReasonForExpensesHandler = this.changeReasonForExpensesHandler.bind(this);
        this.saveOrUpdateExpenses = this.saveOrUpdateExpenses.bind(this);
    }
    
    // step 3
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            ExpensesService.getExpensesById(this.state.id).then( (res) =>{
                let expenses = res.data;
                this.setState({
                    expensesName: expenses.expensesName,
                    amount: expenses.amount,
                    date : expenses.date,
                    reasonForExpenses : expenses.reasonForExpenses
                });
            });
        }        
    }
    saveOrUpdateExpenses = (e) => {
        e.preventDefault();
        let expenses = {expensesName: this.state.expensesName, amount: this.state.amount, date: this.state.date, reasonForExpenses: this.state.reasonForExpenses};
        console.log('expenses => ' + JSON.stringify(expenses));

        // step 5
        if(this.state.id === '_add'){
            ExpensesService.createExpenses(expenses).then(res =>{
                this.props.history.push('/expenses');
            });
        }else{
            ExpensesService.updateExpenses(expenses, this.state.id).then( res => {
                this.props.history.push('/expenses');
            });
        }
    }
    
    changeExpensesNameHandler= (event) => {
        this.setState({expensesName: event.target.value});
    }

    changeAmountHandler= (event) => {
        this.setState({amount: event.target.value});
    }

    changeDateHandler= (event) => {
        this.setState({date: event.target.value});
    }

    changeReasonForExpensesHandler= (event) => {
        this.setState({reasonForExpenses: event.target.value});
    }

    cancel(){
        this.props.history.push('/expenses');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Expenses</h3>
        }else{
            return <h3 className="text-center">Update Expenses</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    this.getTitle()
                                }
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Expenses Name : </label>
                                            <input placeholder="Expenses Name" name="expensesName" className="form-control" 
                                                value={this.state.expensesName} onChange={this.changeExpensesNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Amount: </label>
                                            <input placeholder="Amount" name="amount" className="form-control" 
                                                value={this.state.amount} onChange={this.changeAmountHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Date: </label>
                                            <input placeholder="Date" name="date" className="form-control" 
                                                value={this.state.date} onChange={this.changeDateHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Reason For Expenses: </label>
                                            <input placeholder="Reason For Expenses" name="reasonForExpenses" className="form-control" 
                                                value={this.state.reasonForExpenses} onChange={this.changeReasonForExpensesHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.saveOrUpdateExpenses}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default CreateExpensesComponent
