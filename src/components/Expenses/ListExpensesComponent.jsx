import MaterialTable from 'material-table';
import React, { useState, useEffect } from 'react';

function ListExpensesComponent() {
   
        const [data, setData] = useState([])
        useEffect(() => {
            fetch("https://api.coingecko.com/api/v3/coins/markets?vs_currency=USD&order=market_cap_desc&per_page=100&page=1&sparkline=false")
              .then(resp => resp.json())
              .then(resp => {
                setData(resp)
              })
          }, [])

        return (
         
     <div style={{ maxWidth: '100%' }}>
        <MaterialTable
        options={{
            exportButton: true
          }}
          columns={[
            { title: 'Image', field: 'image',
            render: rowData => <img src={rowData.image} style={{width: 50, borderRadius: '50%'}}/>},
            { title: 'Name', field: 'name' },
            { title: 'Current Price', field: 'current_price' },
            { title: 'Market Cap', field: 'market_cap' },
            { title: 'High 24h', field: 'high_24h' },
            { title: 'Low 24h', field: 'low_24h' }
            
          ]}
          
          data={data}
          title="Bitcoin List"
        />
      </div>
 
        )
    }
export default ListExpensesComponent
