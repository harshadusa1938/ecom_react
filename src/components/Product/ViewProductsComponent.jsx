import React, { Component } from "react";
import ProductsService from "../../services/ProductsService";
import "./Product.css";
import Button from '@material-ui/core/Button';
import ProductDetails from "../Product/ProductDetailsTabs";
import Sidebar from "../Product/Sidebar";
class ViewProductsComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.match.params.id,
      products: {},
    };
  }

  componentDidMount() {
    ProductsService.getProductsById(this.state.id).then((res) => {
      this.setState({ products: res.data });
    });
  }

  render() {
    return (
      <div className="page-main">

        <div className="row">
          <div class="col-6">
            <img src="https://images-na.ssl-images-amazon.com/images/I/31IvZjapERL._AC_.jpg" />
          </div>
          <div class="col-6">
            <div className="row">
              <div className="row_value">
                {" "}
               <h3> {this.state.products.productName} </h3>
              </div>
            </div>
            <div className="row">
              <div> ${this.state.products.productPrice}</div>
            </div>
            <div className="row">
              <label> Size: </label>
              <div> {this.state.products.productSize}</div>
            </div>
            <div className="row">
              <label> Color: </label>
              <div> {this.state.products.productColor}</div>
            </div>

            <Button variant="contained" color="primary">
               Add to Cart
            </Button>
          </div>
          {/* <ProductDetails/> */}
        </div>

        <hr/>
        <div className="related-products"> 
            <h3> Related Products </h3>

        </div>


        <hr/>
        <div className="related-products"> 
            <h3> Related Products </h3>

        </div>
      </div>
    );
  }
}

export default ViewProductsComponent;
