import React, { Component } from "react";
import ProductsService from "../../services/ProductsService";
import { Card, CardContent, Typography } from '@material-ui/core';
import "./Product.css"; 
import Button from '@material-ui/core/Button';
class ListProductsComponent extends Component {
    

  constructor(props) {
    super(props);

    this.state = {
      products: [],
    };
  }

  viewProducts(id) {
    this.props.history.push(`/view-products/${id}`);
  }

  componentDidMount() {
    ProductsService.getProducts().then((res) => {
      this.setState({ products: res.data });
    });
  }

  render() {
      
    return (
        
      <div className="page-background">
        <h2 className="text-center">Product List</h2>
        <br></br>

        {this.state.products.map((product) => (
          <Card>
            <CardContent>
              <div>
              <img src="https://wallpapercave.com/wp/fZa3Jab.jpg" height='200px' width='200px' onClick={ () => this.viewProducts(product.id)}/>
                <Typography variant="h5">{product.productName}</Typography>
              
                <Typography variant="h5">${product.productPrice}</Typography>

                   {/* <div className = "row">
                      <button style={{marginLeft: "15px"}} onClick={ () => this.viewProducts(product.id)} className="btn btn-primary">Add to Cart </button>
                   </div> */}

                   <Button variant="contained" color="primary" onClick={ () => this.viewProducts(product.id)}>
                      Add to Cart
                   </Button>
              </div>
            </CardContent>
          </Card>
        ))}
      </div>
    );
  }
}

export default ListProductsComponent;
