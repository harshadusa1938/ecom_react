import React, { Component } from "react";
import TaskDataService from "../../services/task.service";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import "./ListTask.css";

const LOCAL_API_URL = "http://localhost:8081";
//const TASK_PAGE = 'tms/v1/api/tasks'
//const GET_ALL_TASK = 'getAllTask'

//const INSTRUCTOR_API_URLs = `${LOCAL_API_URL}/${TASK_PAGE}/${GET_ALL_TASK}`

//const DELETE_TASK_API = `${TASK_PAGE}/deleteTask`
//const CREATE_TASK_API = `${TASK_PAGE}/createTask`

class ListTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      message: null,
    };
    this.deleteTask = this.deleteTask.bind(this);
    this.editTask = this.editTask.bind(this);
    this.addTask = this.addTask.bind(this);
    this.refreshTask = this.refreshTask.bind(this);
  }

  componentDidMount() {
    this.refreshTask();
  }
  refreshTask() {
    TaskDataService.retrieveAllTask(LOCAL_API_URL) //HARDCODED
      .then((response) => {
        // console.log(response);
        this.setState({ tasks: response.data });
      });
  }

  deleteTask(id) {
    TaskDataService.deleteTask(id).then((response) => {
      this.setState({ message: `Delete of Task ${id} Successful` });
      this.setState({
        tasks: this.state.tasks.filter((task) => task.id !== id),
      });
      this.refreshTask();
    });
  }

  addTask() {
    this.props.history.push(`/add-task`);
  }

  editTask(id) {
    // window.localStorage.setItem("id", id);
    this.props.history.push(`/edit-task/${id}`);
  }

  viewTask(id){
    this.props.history.push(`/view-task/${id}`);
}

  render() {
    console.log("render");
    return (
        
      <div className="container">
        <h3>All Tasks</h3>
        <div className="row">
          <button className="btn btn-success" onClick={this.addTask}> Add </button>
        </div>
        
        {this.state.message && (
          <div className="alert alert-success">{this.state.message}</div>
        )}
        <div className="listtask__body">
          <MDBTable>
            <MDBTableHead color="primary-color">
              <tr>
                <th>Task Name</th>
                <th>Task Description</th>
                <th>Date</th>
                <th>Task Status</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>View</th>
              </tr>
            </MDBTableHead>
            <MDBTableBody>
              {this.state.tasks.map((task) => (
                <tr key={task.id}>
                  <td>{task.taskName}</td>
                  <td>{task.taskDescription}</td>
                  <td>{task.date}</td>
                  <td>{task.taskStatus}</td>

                  <td>
                    <button className="btn btn-success" onClick={() => this.editTask(task.id)}>Edit </button>
                  </td>
                  <td>
                    <button className="btn btn-danger" onClick={() => this.deleteTask(task.id)}> Delete </button>
                  </td>
            
                <td>  <button style={{marginLeft: "10px"}} onClick={ () => this.viewTask(task.id)} className="btn btn-info">View </button></td>
                </tr>
              ))}
            </MDBTableBody>
            </MDBTable>
        </div>
      </div>
    );
  }
}
export default ListTask;
