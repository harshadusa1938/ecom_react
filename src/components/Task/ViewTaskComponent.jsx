import React, { Component } from 'react';
import TaskDataService from "../../services/task.service";
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody } from "mdbreact";
import { Link } from "react-router-dom";

class ViewTaskComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            id: this.props.match.params.id,
            task: {}
        }
    }

    componentDidMount(){
        TaskDataService.getTask(this.state.id).then( res => {
            this.setState({task: res.data});
        })
    }

    goToBack() {
        this.props.history.push("/task");
      }

    render() {
        return(
            <div>
                <h2 className="text-center">View Task</h2>
                <MDBContainer>
                 <MDBRow>
                     <MDBCol md="12">
                           <MDBCard>
                            <MDBCardBody className="mx-2"> 
                <form>
                
                <div className="form-group">
                    <label>Task Name</label>
                    <div> { this.state.task.taskName }</div>
                </div>

                <div className="form-group">
                    <label>Task Name</label>
                    <div> { this.state.task.taskStatus }</div>
                </div>

                <div className="form-group">
                    <label>Task Description</label>
                    <div> { this.state.task.Date }</div>
                </div>

                <div className="form-group">
                    <label>Task Name</label>
                    <div> { this.state.task.taskStatus }</div>
                </div>

                <div className="form-group">
                    <label>Task Name</label>
                    <div> { this.state.task.taskStatus }</div>
                </div>
                <Link to={"/task"} className="nav-link">
                  Back
                </Link>
              
                
            </form>
            </MDBCardBody> </MDBCard></MDBCol></MDBRow></MDBContainer>
    </div>
        );
    }
}

export default ViewTaskComponent;