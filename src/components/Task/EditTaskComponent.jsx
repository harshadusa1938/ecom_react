import React, { Component } from 'react'
import TaskDataService from "../../services/task.service";
import { MDBInput, MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody } from "mdbreact";

class EditTaskComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: this.props.match.params.id,
            taskName: '',
            taskDescription: '',
            date: '',
            taskStatus: '',
        }
        this.changeTaskNameHandler = this.changeTaskNameHandler.bind(this);
        this.changeTaskDescriptionHandler = this.changeTaskDescriptionHandler.bind(this);
        this.changeDateHandler = this.changeDateHandler.bind(this); 
        this.changeTaskStatusHandler = this.changeTaskStatusHandler.bind(this);
    }

    componentDidMount() {
       // this.loadTask();
       // step 4
       if(this.state.id === 'task.id'){
            return
        }else{
            TaskDataService.getTask(this.state.id).then( (res) =>{
                let task = res.data;
                this.setState({
                    taskName: task.taskName,
                    taskDescription: task.taskDescription,
                    date: task.date,
                    taskStatus: task.taskStatus
                });
            });
        }     
    }

    saveOrUpdateEmployee = (e) => {
        e.preventDefault();
        let task = {taskName: this.state.taskName, taskDescription: this.state.taskDescription, date: this.state.date, taskStatus: this.state.taskStatus};
        console.log('task => ' + JSON.stringify(task));

        // step 5
        if(this.state.id === '_add'){
            TaskDataService.addTask(task).then(res =>{
                this.props.history.push('/task');
            });
        }else{
            TaskDataService.editTask(task, this.state.id).then( res => {
                this.props.history.push('/task');
            });
        }
    }
    
    changeTaskNameHandler= (event) => {
        this.setState({taskName: event.target.value});
    }

    changeTaskDescriptionHandler= (event) => {
        this.setState({taskDescription: event.target.value});
    }

    changeDateHandler= (event) => {
        this.setState({date: event.target.value});
    }

    changeTaskStatusHandler= (event) => {
        this.setState({taskStatus: event.target.value});
    }

    cancel(){
        this.props.history.push('/task');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Task</h3>
        }else{
            return <h3 className="text-center">Update Task</h3>
        }
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Edit Task</h2>
                <MDBContainer>
                 <MDBRow>
                     <MDBCol md="12">
                           <MDBCard>
                            <MDBCardBody className="mx-2"> 

                <form>

                     <div className="form-group">  
                        <label>Task Id</label>                            
                        <MDBInput background size="sm" name="id" className="form-control" readOnly={true} value={this.state.id}/>
                    </div>

                    <div className="form-group">    
                        <label>Task Name</label>                   
                        <MDBInput type="text" background size="sm" name="taskName" className="form-control" value={this.state.taskName} onChange={this.onChange}/>
                    </div>

                    <div className="form-group"> 
                    <label>Task Description</label>                       
                        <MDBInput type="text" background size="sm" name="taskDescription" className="form-control" value={this.state.taskDescription} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">  
                        <label>Date</label>                      
                        <MDBInput type="text" background size="sm" name="date" className="form-control" value={this.state.date} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">  
                        <label>Task Status</label>                    
                        <MDBInput type="text" background size="sm" name="taskStatus" className="form-control" value={this.state.taskStatus} onChange={this.onChange}/>
                    </div>
                  
                    <button className="btn btn-success" onClick={this.saveTask}>Save</button>
                </form>
                </MDBCardBody> </MDBCard></MDBCol></MDBRow></MDBContainer>
            </div>
        );
    }
}

export default EditTaskComponent;