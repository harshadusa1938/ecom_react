import React, { Component } from 'react'
import TaskDataService from "../../services/task.service";
import { MDBInput, MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody } from "mdbreact";

class AddTaskComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            taskName: '',
            taskDescription: '',
            date: '',
            taskStatus: '',
            message: null
        }
        this.saveTask = this.saveTask.bind(this);
    }

    saveTask = (e) => {
        e.preventDefault();
        let task = {taskName: this.state.taskName, taskDescription: this.state.taskDescription, date: this.state.date, taskStatus: this.state.taskStatus};
        TaskDataService.addTask(task)
        .then(res => {
                this.setState({message : 'Task added successfully.'});
                this.props.history.push('/task');
            });
           console.log(task);
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <div>
                <h2 className="text-center">Add Task</h2>
                <MDBContainer>
                 <MDBRow>
                     <MDBCol md="12">
                           <MDBCard>
                            <MDBCardBody className="mx-2"> 
                <form>
                
                <div className="form-group">
                    <label>Task Name</label>
                    <MDBInput type="text" background size="sm" name="taskName" className="form-control" value={this.state.taskName} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Task Description</label>
                    <MDBInput type="text" background size="sm"  name="taskDescription" className="form-control" value={this.state.taskDescription} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Date</label>
                    <MDBInput type="text" background size="sm"  name="date" className="form-control" value={this.state.date} onChange={this.onChange}/>
                </div>
            
                <div className="form-group">
                    <label>Task Status</label>
                    <MDBInput type="text"  background size="sm"  name="taskStatus" className="form-control" value={this.state.taskStatus} onChange={this.onChange}/>
                </div>
                   
                <button className="btn btn-success" onClick={this.saveTask}>Save</button>
                
            </form>
            </MDBCardBody> </MDBCard></MDBCol></MDBRow></MDBContainer>
    </div>
        );
    }
}

export default AddTaskComponent;