import { RotateLeftSharp } from '@material-ui/icons';
import { getRoles } from '@testing-library/react';
import React, { Component } from 'react'
import UsersService from '../../services/UsersService'

class ListUsersComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            users: []
        }
        this.deleteUser = this.deleteUser.bind(this);
    }

    deleteUser(id){
        UsersService.deleteUser(id).then( res => {
            this.setState({users: this.state.users.filter(user => user.id !== id)});
        });
    }

    componentDidMount(){
        UsersService.getUsers().then((res) => {
            this.setState({ users: res.data});
        });
    }

    render() {
        return (
            <div>
                 <h2 className="text-center">Users List</h2>
                 
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> Id</th>
                                    <th> Username</th>
                                    <th> Email</th>
                                    <th> Roles Rights</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.users.map(
                                        user => 
                                        <tr key = {user.id}>
                                             <td> {user.id} </td>
                                             <td> {user.username} </td>
                                             <td> {user.email} </td>   
                                             <td> {user.roles.map(role => role.name).join(' | ')}</td>
                                             {/* <td>{user.roles.forEach(role=> role.name).join(',')}</td> */}
                                             <button style={{marginLeft: "10px"}} onClick={ () => this.deleteEmployee(user.id)} className="btn btn-danger">Delete </button>                              
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListUsersComponent
