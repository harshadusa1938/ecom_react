import React, { Component } from 'react'
import OfficeExpesesMasterService from '../../services/OfficeExpesesMasterService'

class ListOfficeExpensesMasterComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            officeExpensesMasters: []
        }
        this.deleteOfficeExpensesMaster = this.deleteOfficeExpensesMaster.bind(this);
    }

    deleteOfficeExpensesMaster(id){
        OfficeExpesesMasterService.deleteExpenses(id).then( res => {
            this.setState({officeExpensesMasters: this.state.officeExpensesMasters.filter(user => user.id !== id)});
        });
    }

    componentDidMount(){
        OfficeExpesesMasterService.getExpenses().then((res) => {
            this.setState({ officeExpensesMasters: res.data});
        });
    }

    render() {
        return (
            <div>
                 <h2 className="text-center">Office Expenses Master List</h2>
                 
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> Id</th>
                                    <th> Payment Mode</th>
                                    <th> Description</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.officeExpensesMasters.map(
                                        officeExpensesMaster => 
                                        <tr key = {officeExpensesMaster.id}>
                                             <td> {officeExpensesMaster.id} </td>
                                             <td> {officeExpensesMaster.paymentMode} </td>    
                                             <td> {officeExpensesMaster.description} </td>                                        
                                             <button style={{marginLeft: "10px"}} onClick={ () => this.deleteOfficeExpensesMaster(officeExpensesMaster.id)} className="btn btn-danger">Delete </button>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                 </div>
            </div>
        )
    }
}

export default ListOfficeExpensesMasterComponent
