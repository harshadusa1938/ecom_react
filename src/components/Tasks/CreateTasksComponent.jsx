import React, { Component } from 'react'
import TasksService from '../../services/TasksService';

class CreateTasksComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            taskName: '',
            taskDescription: '',
            date: '',
            taskStatus: ''
        }
        this.changeTaskNameHandler = this.changeTaskNameHandler.bind(this);
        this.changeTaskDescriptionHandler = this.changeTaskDescriptionHandler.bind(this);
        this.changeDateHandler = this.changeDateHandler.bind(this);
        this.changeTaskStatussHandler = this.changeTaskStatussHandler.bind(this);
        this.saveOrUpdateTasks = this.saveOrUpdateTasks.bind(this);
    }
    
    // step 3
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            TasksService.getTasksById(this.state.id).then( (res) =>{
                let tasks = res.data;
                this.setState({
                    taskName: tasks.taskName,
                    taskDescription: tasks.taskDescription,
                    date : tasks.date,
                    taskStatus : tasks.taskStatus
                });
            });
        }        
    }
    saveOrUpdateTasks = (e) => {
        e.preventDefault();
        let tasks = {taskName: this.state.taskName, taskDescription: this.state.taskDescription, date: this.state.date, taskStatus: this.state.taskStatus};
        console.log('tasks => ' + JSON.stringify(tasks));

        // step 5
        if(this.state.id === '_add'){
            TasksService.createTasks(tasks).then(res =>{
                this.props.history.push('/tasks');
            });
        }else{
            TasksService.updateTasks(tasks, this.state.id).then( res => {
                this.props.history.push('/tasks');
            });
        }
    }
    
    changeTaskNameHandler= (event) => {
        this.setState({taskName: event.target.value});
    }

    changeTaskDescriptionHandler= (event) => {
        this.setState({taskDescription: event.target.value});
    }

    changeDateHandler= (event) => {
        this.setState({date: event.target.value});
    }

    changeTaskStatussHandler= (event) => {
        this.setState({taskStatus: event.target.value});
    }

    cancel(){
        this.props.history.push('/tasks');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Tasks</h3>
        }else{
            return <h3 className="text-center">Update Tasks</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    this.getTitle()
                                }
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Task Name : </label>
                                            <input placeholder="Tasks Name" name="taskName" className="form-control" 
                                                value={this.state.taskName} onChange={this.changeTaskNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Task Description: </label>
                                            <input placeholder="Task Description" name="taskDescription" className="form-control" 
                                                value={this.state.taskDescription} onChange={this.changeTaskDescriptionHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Date: </label>
                                            <input placeholder="Date" name="date" className="form-control" 
                                                value={this.state.date} onChange={this.changeDateHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Task Status: </label>
                                            <input placeholder="Task Status" name="taskStatus" className="form-control" 
                                                value={this.state.taskStatus} onChange={this.changeTaskStatussHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.saveOrUpdateTasks}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default CreateTasksComponent
