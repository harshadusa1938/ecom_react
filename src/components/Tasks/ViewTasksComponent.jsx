import React, { Component } from 'react'
import TasksService from '../../services/TasksService'

class ViewTasksComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            tasks: {}
        }
    }

    componentDidMount(){
        TasksService.getTasksById(this.state.id).then( res => {
            this.setState({tasks: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Tasks Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> Task Name: </label>
                            <div> { this.state.tasks.taskName }</div>
                        </div>
                        <div className = "row">
                            <label> Task Description: </label>
                            <div> { this.state.tasks.taskDescription }</div>
                        </div>
                        <div className = "row">
                            <label> Date: </label>
                            <div> { this.state.tasks.date }</div>
                        </div>
                        <div className = "row">
                            <label> Task Status: </label>
                            <div> { this.state.tasks.taskStatus }</div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default ViewTasksComponent
