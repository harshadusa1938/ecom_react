import React, { Component } from 'react'
import TasksService from '../../services/TasksService'

class ListTasksComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            tasks: []
        }
        this.addTasks = this.addTasks.bind(this);
        this.editTasks = this.editTasks.bind(this);
        this.deleteTasks = this.deleteTasks.bind(this);
    }

    deleteTasks(id){
        TasksService.deleteTasks(id).then( res => {
            this.setState({tasks: this.state.tasks.filter(tasks => tasks.id !== id)});
       });
     }
    viewTasks(id){
        this.props.history.push(`/view-tasks/${id}`);
    }
    editTasks(id){
        this.props.history.push(`/add-tasks/${id}`);
    }

    componentDidMount(){
        TasksService.getTasks().then((res) => {
            this.setState({ tasks: res.data});
        });
    }
     addTasks(){
         this.props.history.push('/add-tasks/_add');
     }

    render() {
        return (
            <div>
                 <h2 className="text-center">Tasks List</h2>
                 <div className = "row">
                    <button className="btn btn-primary" onClick={this.addTasks}> Add Tasks</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> Task Name</th>
                                    <th> Task Description</th>
                                    <th> Date</th>
                                    <th> Task Status</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.tasks.map(
                                        task => 
                                        <tr key = {task.id}>
                                             <td> {task.taskName} </td>
                                             <td> {task.taskDescription} </td>
                                             <td> {task.date} </td>   
                                             <td> {task.taskStatus} </td> 
                                             <td>
                                                 <button onClick={ () => this.editTasks(task.id)} className="btn btn-success">Update </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteTasks(task.id)} className="btn btn-danger">Delete </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewTasks(task.id)} className="btn btn-info">View </button>
                                             </td>                                                                 
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListTasksComponent
